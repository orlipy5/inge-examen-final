<?php

namespace App\Clases;

use Exception;

class AuthException extends Exception
{
    public function mostrarMensajePersonalizado()
    {
        return "Error de autenticación: " . $this->getMessage();
    }
}
