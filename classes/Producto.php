<?php

namespace App\Clases;

class Producto
{
    private $id;
    private $nombre;
    private $codigo;
    private $marca;
    private $precio;

    // Constructor
    public function __construct($id, $nombre, $codigo, $marca, $precio)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->codigo = $codigo;
        $this->marca = $marca;
        $this->precio = $precio;
    }

    // Métodos getter
    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function getMarca()
    {
        return $this->marca;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    // Métodos setter
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    public function setMarca($marca)
    {
        $this->marca = $marca;
    }

    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }
}
