<?php

namespace App\Clases;

class Usuario
{
    private $id;
    private $nombre;
    private $correo;
    private $contrasena;

    // Constructor
    public function __construct($id, $nombre, $correo, $contrasena)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->correo = $correo;
        $this->contrasena = $contrasena;
    }

    // Métodos getter
    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    // Métodos setter
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function setCorreo($correo)
    {
        $this->correo = $correo;
    }

}
