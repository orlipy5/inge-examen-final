-- Crear la tabla de usuario
CREATE TABLE IF NOT EXISTS usuario (
                                       id SERIAL PRIMARY KEY,
                                       nombre VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    contrasena VARCHAR(255) NOT NULL
    );

-- Crear la tabla de producto
CREATE TABLE IF NOT EXISTS producto (
                                        id SERIAL PRIMARY KEY,
                                        nombre VARCHAR(255) NOT NULL,
    codigo VARCHAR(50) NOT NULL,
    marca VARCHAR(100) NOT NULL,
    precio NUMERIC(10, 2) NOT NULL
    );

-- Insertar usuarios
INSERT INTO usuario (nombre, email, contrasena) VALUES
                                                    ('Usuario1', 'usuario1@example.com', 'contrasena1'),
                                                    ('Usuario2', 'usuario2@example.com', 'contrasena2'),
                                                    ('Usuario3', 'usuario3@example.com', 'contrasena3');

-- Insertar productos
INSERT INTO producto (nombre, codigo, marca, precio) VALUES
                                                         ('Producto1', 'ABC123', 'Marca1', 19.99),
                                                         ('Producto2', 'XYZ789', 'Marca2', 29.99),
                                                         ('Producto3', '123DEF', 'Marca3', 39.99);
