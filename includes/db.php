<?php

namespace App\Includes;

class Database
{
    private $host = 'localhost';
    private $username = 'postgres';
    private $password = 'postgres';
    private $database = 'inge_final';
    private $conn;

    public function __construct()
    {
        try {
            $dsn = "pgsql:host={$this->host};dbname={$this->database};user={$this->username};password={$this->password}";
            $this->conn = new PDO($dsn);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die('Error de conexión a la base de datos: ' . $e->getMessage());
        }
    }

    public function getConnection()
    {
        return $this->conn;
    }
}

$db = new Database();
$conn = $db->getConnection();
