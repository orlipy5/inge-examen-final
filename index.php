<?php
require_once __DIR__ . '/includes/header.php';
require_once __DIR__ . '/includes/footer.php';
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página Principal</title>
</head>
<body>
<h2>Bienvenido a la Página Principal</h2>

<?php
session_start();

if (isset($_SESSION['usuario'])) {
    $usuario = $_SESSION['usuario'];
    echo "<p>Bienvenido, {$usuario->getNombre()}!</p>";
} else {
    echo "<p>Inicia sesión para acceder a todas las funciones.</p>";
    echo "<p><a href='login.php'>Ir al Formulario de Inicio de Sesión</a></p>";
}
?>

<!-- Agrega aquí otros elementos HTML o enlaces -->

<?php include_once __DIR__ . '/includes/footer.php'; ?>
</body>
</html>
