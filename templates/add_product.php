<?php include_once __DIR__ . '/includes/header.php'; ?>

<?php

use MiProyecto\Clases\Productos;

global $conn;
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../includes/db.php';
require_once __DIR__ . '/../classes/Producto.php';

if (!isset($_SESSION['usuario'])) {
    header("Location: login.php");
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nombre = $_POST['nombre'];
    $codigo = $_POST['codigo'];
    $marca = $_POST['marca'];
    $precio = $_POST['precio'];


    $stmt = $conn->prepare("INSERT INTO productos (nombre, codigo, marca, precio) VALUES (:nombre, :codigo, :marca, :precio)");
    $stmt->bindParam(':nombre', $nombre);
    $stmt->bindParam(':codigo', $codigo);
    $stmt->bindParam(':marca', $marca);
    $stmt->bindParam(':precio', $precio);

    if ($stmt->execute()) {
        header("Location: product_list.php");
        exit;
    } else {
        $error = "Error al agregar el producto";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Agregar Nuevo Producto</title>
</head>
<body>
<h2>Agregar Nuevo Producto</h2>

<?php if (isset($error)): ?>
    <p style="color: red;"><?php echo $error; ?></p>
<?php endif; ?>

<form action="add_product.php" method="post">
    <label for="nombre">Nombre:</label>
    <input type="text" id="nombre" name="nombre" required>

    <label for="codigo">Código:</label>
    <input type="text" id="codigo" name="codigo" required>

    <label for="marca">Marca:</label>
    <input type="text" id="marca" name="marca" required>

    <label for="precio">Precio:</label>
    <input type="number" id="precio" name="precio" step="0.01" required>

    <button type="submit">Agregar Producto</button>
</form>

<p><a href="product_list.php">Volver al listado de productos</a></p>

</body>
</html>

<?php include_once __DIR__ . '/includes/footer.php'; ?>
