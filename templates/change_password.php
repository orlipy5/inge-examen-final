<?php include_once __DIR__ . '/includes/header.php'; ?>

<?php
require_once __DIR__ . '/includes/header.php';
require_once __DIR__ . '/includes/db.php';
require_once __DIR__ . '/classes/Usuario.php';
require_once __DIR__ . '/classes/AuthException.php';

session_start();

if (!isset($_SESSION['usuario'])) {
    header("Location: login.php");
    exit;
}

$usuario = $_SESSION['usuario'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try {
        $oldPassword = $_POST['old_password'];
        $newPassword = $_POST['new_password'];

        if (!$usuario->verificarContrasena($oldPassword)) {
            throw new AuthException("La contraseña actual no es válida");
        }

        $usuario->cambiarContrasena($newPassword);

        header("Location: index.php");
        exit;

    } catch (AuthException $e) {
        $error = $e->getMessage();
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cambiar Contraseña</title>
    <!-- Agrega aquí tus enlaces a CSS u otros recursos -->
</head>
<body>
<h2>Cambiar Contraseña</h2>

<?php if (isset($error)): ?>
    <p style="color: red;"><?php echo $error; ?></p>
<?php endif; ?>

<form action="change_password.php" method="post">
    <label for="old_password">Contraseña Actual:</label>
    <input type="password" id="old_password" name="old_password" required>

    <label for="new_password">Nueva Contraseña:</label>
    <input type="password" id="new_password" name="new_password" required>

    <label for="confirm_password">Confirmar Nueva Contraseña:</label>
    <input type="password" id="confirm_password" name="confirm_password" required>

    <button type="submit">Cambiar Contraseña</button>
</form>

<p><a href="index.php">Volver al Inicio</a></p>

</body>
</html>

<?php include_once __DIR__ . '/includes/footer.php'; ?>