<?php include_once __DIR__ . '/includes/header.php'; ?>

<?php
use MiProyecto\Clases\Productos;

global $conn;
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../includes/db.php';
require_once __DIR__ . '/../classes/Producto.php';

if (!isset($_SESSION['usuario'])) {
    header("Location: login.php");
    exit;
}

if (!isset($_GET['id'])) {
    header("Location: product_list.php");
    exit;
}

$idProducto = $_GET['id'];

$stmt = $conn->prepare("SELECT * FROM productos WHERE id = :id");
$stmt->bindParam(':id', $idProducto);
$stmt->execute();
$producto = $stmt->fetch(PDO::FETCH_ASSOC);

if (!$producto) {
    header("Location: product_list.php");
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nombre = $_POST['nombre'];
    $codigo = $_POST['codigo'];
    $marca = $_POST['marca'];
    $precio = $_POST['precio'];


    $stmt = $conn->prepare("UPDATE productos SET nombre = :nombre, codigo = :codigo, marca = :marca, precio = :precio WHERE id = :id");
    $stmt->bindParam(':nombre', $nombre);
    $stmt->bindParam(':codigo', $codigo);
    $stmt->bindParam(':marca', $marca);
    $stmt->bindParam(':precio', $precio);
    $stmt->bindParam(':id', $idProducto);

    if ($stmt->execute()) {
        header("Location: product_list.php");
        exit;
    } else {
        $error = "Error al editar el producto";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Producto</title>
</head>
<body>
<h2>Editar Producto</h2>

<?php if (isset($error)): ?>
    <p style="color: red;"><?php echo $error; ?></p>
<?php endif; ?>

<form action="edit_product.php?id=<?php echo $idProducto; ?>" method="post">
    <label for="nombre">Nombre:</label>
    <input type="text" id="nombre" name="nombre" value="<?php echo $producto['nombre']; ?>" required>

    <label for="codigo">Código:</label>
    <input type="text" id="codigo" name="codigo" value="<?php echo $producto['codigo']; ?>" required>

    <label for="marca">Marca:</label>
    <input type="text" id="marca" name="marca" value="<?php echo $producto['marca']; ?>" required>

    <label for="precio">Precio:</label>
    <input type="number" id="precio" name="precio" step="0.01" value="<?php echo $producto['precio']; ?>" required>

    <button type="submit">Guardar Cambios</button>
</form>

<p><a href="product_list.php">Volver al listado de productos</a></p>

</body>
</html>

<?php include_once __DIR__ . '/includes/footer.php'; ?>
