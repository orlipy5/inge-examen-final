<?php include_once __DIR__ . '/includes/header.php'; ?>

<?php
use MiProyecto\Clases\Usuario;

session_start();

if (!isset($_SESSION['usuario'])) {
    header("Location: login.php");
    exit;
}

$usuario = $_SESSION['usuario'];

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página Principal</title>
</head>
<body>
<h2>Bienvenido, <?php echo $usuario->getNombre(); ?>!</h2>

<p>Esta es tu página principal.</p>

<a href="logout.php">Cerrar Sesión</a>
</body>
</html>

<?php include_once __DIR__ . '/includes/footer.php'; ?>
