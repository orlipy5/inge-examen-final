<?php include_once __DIR__ . '/includes/header.php'; ?>

<?php

use MiProyecto\Clases\Productos;

global $conn;
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../includes/db.php';
require_once __DIR__ . '/../classes/Producto.php';

if (!isset($_SESSION['usuario'])) {
    header("Location: login.php");
    exit;
}

$stmt = $conn->query("SELECT * FROM productos");
$productos = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listado de Productos</title>
</head>
<body>
<h2>Listado de Productos</h2>

<table border="1">
    <thead>
    <tr>
        <th>Nombre</th>
        <th>Código</th>
        <th>Marca</th>
        <th>Precio</th>
        <th>Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($productos as $producto): ?>
        <tr>
            <td><?php echo $producto['nombre']; ?></td>
            <td><?php echo $producto['codigo']; ?></td>
            <td><?php echo $producto['marca']; ?></td>
            <td><?php echo $producto['precio']; ?></td>
            <td>
                <a href="view_product.php?id=<?php echo $producto['id']; ?>">Ver</a> |
                <a href="edit_product.php?id=<?php echo $producto['id']; ?>">Editar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<p><a href="add_product.php">Agregar nuevo producto</a></p>

</body>
</html>

<?php include_once __DIR__ . '/includes/footer.php'; ?>
