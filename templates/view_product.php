<?php include_once __DIR__ . '/includes/header.php'; ?>

<?php

use MiProyecto\Clases\Productos;

global $conn;
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../includes/db.php';
require_once __DIR__ . '/../classes/Producto.php';

if (!isset($_SESSION['usuario'])) {
    header("Location: login.php");
    exit;
}

if (!isset($_GET['id'])) {
    header("Location: product_list.php");
    exit;
}

$idProducto = $_GET['id'];

$stmt = $conn->prepare("SELECT * FROM productos WHERE id = :id");
$stmt->bindParam(':id', $idProducto);
$stmt->execute();
$producto = $stmt->fetch(PDO::FETCH_ASSOC);

if (!$producto) {
    header("Location: product_list.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detalles del Producto</title>
</head>
<body>
<h2>Detalles del Producto</h2>

<p><strong>Nombre:</strong> <?php echo $producto['nombre']; ?></p>
<p><strong>Código:</strong> <?php echo $producto['codigo']; ?></p>
<p><strong>Marca:</strong> <?php echo $producto['marca']; ?></p>
<p><strong>Precio:</strong> <?php echo $producto['precio']; ?></p>

<p><a href="product_list.php">Volver al listado de productos</a></p>

</body>
</html>

<?php include_once __DIR__ . '/includes/footer.php'; ?>
